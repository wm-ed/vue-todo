const path = require('path')

module.exports = {
    chainWebpack: config => {
        const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
        types.forEach(type => addStyleResource(config.module.rule('scss').oneOf(type)))
    },
    "transpileDependencies": ["vuetify"],
    pluginOptions: {
        svgSprite: {
            dir: 'src/assets/icons',
            test: /\.(svg)(\?.*)?$/,
            loaderOptions: {
                extract: true,
                spriteFilename: 'icons/sprite.[hash:8].svg'
            },
            pluginOptions: {
                plainSprite: true
            }
        }
    }
}

function addStyleResource (rule) {
    rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
        patterns: [
            path.resolve(__dirname, './src/assets/scss/base/mixins.scss'),
            path.resolve(__dirname, './src/assets/scss/base/variables.scss'),
        ],
    })
}