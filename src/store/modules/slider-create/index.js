import mutations from './mutations'
import getters from './getters'
import actions from './actions'

const state = {
    viewed: true,
    data: [
        {
            language: 'ua',
            link: '',
            alt: '',
            title: '',
            colors: {
                select: { abbr: 'light', state: 'Світлий' },
                options: [
                    { abbr: 'light', state: 'Світлий' },
                    { abbr: 'dark', state: 'Темний' },
                ]
            },
            positions: {
                select: {},
                options: [
                    { abbr: 'slide-text--top-left', state: 'Зверху зліва' },
                    { abbr: 'slide-text--top-right', state: 'Зверху справа' },
                ]
            },
            pictures: [
                { img: '', ratio: { width: 640, height: 980 } },
                { img: '', ratio: { width: 1200, height: 700 } },
            ]
        },
        {
            language: 'ru',
            link: '',
            alt: '',
            title: '',
            colors: {
                select: { abbr: 'light', state: 'Світлий' },
                options: [
                    { abbr: 'light', state: 'Світлий' },
                    { abbr: 'dark', state: 'Темний' },
                ]
            },
            positions: {
                select: { abbr: 'slide-text--top-right', state: 'Зверху справа' },
                options: [
                    { abbr: 'slide-text--top-left', state: 'Зверху зліва' },
                    { abbr: 'slide-text--top-right', state: 'Зверху справа' },
                ]
            },
            pictures: [
                { img: '', ratio: { width: 640, height: 980 } },
                { img: '', ratio: { width: 1200, height: 700 } },
            ]
        },
    ]
}

export default {
    state,
    mutations,
    actions,
    getters,
}