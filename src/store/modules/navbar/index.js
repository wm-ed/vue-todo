import mutations from './mutations'
import getters from './getters'
import actions from './actions'

const state = {
    menu: [
        {
            id: 0,
            icon: 'fas fa-home',
            name: 'Головна сторінка',
            submenu: [
                { to: '/sliders', exact: true, name: 'Слайдер на головній' },
                { to: '/tabs-categories', exact: true, name: 'Таби категорій' },
                { to: '/orders', exact: true, name: 'Модуль замовленнь' },
            ]
        },
        {
            id: 1,
            icon: 'fas fa-car',
            name: 'Авто',
            submenu: [
                { to: '/marks', exact: true, name: 'Марки' },
            ]
        },
        {
            id: 2,
            to: '/pages',
            exact: true,
            icon: 'fas fa-heading',
            name: 'Текстові сторінки',
        },
        {
            id: 3,
            to: '/videos',
            exact: true,
            icon: 'fas fa-video',
            name: 'Наші відео',
        },
        {
            id: 4,
            to: '/settings',
            exact: true,
            icon: 'fas fa-cog',
            name: 'Налаштування',
        },
    ]
}

export default {
    state,
    mutations,
    actions,
    getters,
}