import api from '@/api/slider'

const actions = {
    async getSlides ({ state, commit }) {
        try {
            if(!state.data.length) {
                const { data } = await api.slides()
                commit('setSlides', data)
            }
        } catch (error) {
            console.log(error)
        }
    }
}

export default actions
