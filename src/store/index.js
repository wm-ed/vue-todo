import Vue from 'vue'
import Vuex from 'vuex'
import navbar from '@/store/modules/navbar/index'
import sliderCreate from '@/store/modules/slider-create/index'
import sliders from '@/store/modules/slider/index'

Vue.use(Vuex)

const state = {
    _token: 'sd46sdb46b46d4b6f646-dfb54',
    toggleSidebar: false,
    roles: {
        admin: true,
        managers: true,
    }
}

const mutations = {
    setToggleSidebar (state) {
        state.toggleSidebar = !state.toggleSidebar
    }
}

const actions = {}

const getters = {
    getToggleSidebar: state => state.toggleSidebar
}

const modules = {
    navbar,
    sliderCreate,
    sliders,
}

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters,
    modules
})
