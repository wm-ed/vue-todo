import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import Sortable from 'vue-sortable'
import VueMask from 'v-mask'
import { VueMaskDirective } from 'v-mask'
import '@/assets/scss/index.scss'

Vue.config.productionTip = false

Vue.use([Sortable, VueMask])
Vue.directive('mask', VueMaskDirective)

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')
