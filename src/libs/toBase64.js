export const toBase64 = (event) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        if (event.target.files.length) {
            reader.readAsDataURL(event.target.files[0])
            reader.onload = () => { resolve(reader.result) }
        }
    })
}