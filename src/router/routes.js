import Login from '@/pages/login'
import Slider from '@/pages/slider'
import SliderCreate from '@/pages/slider-create'
import TabsCategories from '@/pages/tabs-categories'
import Pages from '@/pages/text-pages'
import Page from '@/pages/text-page'
import Marks from '@/pages/marks'
import OurVideos from '@/pages/our-videos'
import Orders from '@/pages/orders'
import Settings from '@/pages/settings-base'

export const routes = [
    { name: 'Login', path: '/', component: Login, exact: true },
    { name: 'Slider', path: '/sliders', component: Slider, exact: true },
    { name: 'SliderEdit', path: '/slider/:id', component: SliderCreate, props: true, exact: true },
    { name: 'SliderCreate', path: '/slider/create', component: SliderCreate, props: true, exact: true },
    { name: 'TabsCategories', path: '/tabs-categories', component: TabsCategories, exact: true },
    { name: 'Pages', path: '/pages', component: Pages, exact: true },
    { name: 'Page', path: '/page/:id', component: Page, props: true, exact: true },
    { name: 'Marks', path: '/marks', component: Marks, exact: true },
    { name: 'OurVideos', path: '/videos', component: OurVideos, exact: true },
    { name: 'Orders', path: '/orders', component: Orders, exact: true },
    { name: 'Settings', path: '/settings', component: Settings, exact: true },
]