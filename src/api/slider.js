import axios from 'axios'

async function slides () {
    const data = await axios.get('https://jsonplaceholder.typicode.com/photos/10')
    return data
}

export default {
    slides,
}
